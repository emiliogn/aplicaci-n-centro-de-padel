
package centropadel;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


/**
 *
 * @author Emilio García Naranjo
 */
public class CentroPadel 
{


    public static void main(String[] args)
    {
        
        // Vamos a crear un JOptionPanel para hacer login 
        // Creamos el panel contenedor 
        JPanel panelLogin = new JPanel(new BorderLayout(5, 5));
        
        // Panel que contendrá etiquetas
        JPanel etiquetas = new JPanel(new GridLayout(0, 1, 2, 2));
        
        // Etiquetas
        etiquetas.add(new JLabel("Usuario"));
        etiquetas.add(new JLabel("Password"));
        
        // Añadimos el panel con las etiquetas al contenedor
        panelLogin.add(etiquetas, BorderLayout.WEST);
        
        // Panel que contendrá los textFields
        JPanel userPass = new JPanel(new GridLayout(0, 1, 2, 2));
        
        // Creamos textfields
        JTextField usuario = new JTextField();
        userPass.add(usuario);
        JPasswordField password = new JPasswordField();
        userPass.add(password);
        
        // Añadimos el panel con los textfields al contenedor
        panelLogin.add(userPass, BorderLayout.CENTER);
        
        // Llmamos al JOptionPanel con el nuevo panel dentro
        JOptionPane.showMessageDialog(null,panelLogin, "Login", JOptionPane.OK_CANCEL_OPTION);
        
        // Obtenemos el nombre del usuario para utilizarlos mas tarde
        String textUsuario = usuario.getText();
        //String textPassword = password.getPassword();
        
        CentroPadelBaseDDatos cpbd = new CentroPadelBaseDDatos();
        // cargamos la base de datos en el JComboBox correspondiente
        ArrayList<Object[]> datos = cpbd.hacerConsultaUsuarios("SELECT * FROM usuario");
        int i = 0;
        String pass = password.getText();
        for(i = 0; i < datos.size(); i++)
        {
            if(textUsuario.equals(datos.get(i)[1]) && datos.get(i)[5].equals("Trabajador") && datos.get(i)[4].equals(pass))
            {
                CentroPadelFrame cpf = new CentroPadelFrame((String)datos.get(i)[2]);
                break;
            }
        }
        
        //CentroPadelFrame cpf = new CentroPadelFrame("Hola");
        
    }
    
}
