
package centropadel;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Emilio García Naranjo
 */
public class CentroPadelFrame extends JFrame
{
    // Mi base de datos
    CentroPadelBaseDDatos cpbd;
    
    //
    CardLayout cardLayout;
    
    // Paneles 
    JPanel pnCard, pnStatus, pnPrincipal, pnReserva, pnTienda, pnUsuario;
    
    // Etiquetas
    JLabel nombreEmp, fecha;
    
    // Botonoes de la primera pantalla
    JButton btnReserva, btnTienda, btnUsuario, btnSalir;
   
    // Componentes que representaran las pista de padel
    JToggleButton pista1, pista2, pista3, pista4, pista5, pista6, pista7e, pista8e;
    
    // Etiquetas para cuenta atras
    JLabel cAtras1, cAtras2, cAtras3, cAtras4, cAtras5, cAtras6, cAtras7e, cAtras8e;
    
    // Caja donde iran todos los usuarias para poder elegirlos despues
    JComboBox usuariosElegir, tiempoElegir;
    
    // Cajas combos del panel tienda
    JComboBox alimentacion, deportivos;
    
    // Boton Atras(Lo utilizaremos en todas las pantallas) Y limpiar, tambien comun en varias pantallas
    JButton atras, limpiar;
    
    // Modelo tabla para prodcutos
    DefaultTableModel tablaProductosModelo;
    
    // Tablas
    JTable tablaProductos;
    
    // Cajas de texto del panel usuario
    JTextField nick, nombre, telefono, dni;
    JPasswordField password;
    JRadioButton deportista, trabajador;
    
    /**
     * Constructor de nuestra Ventana.
     */
    public CentroPadelFrame(String textUsuario)
    {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // SOlo queremos que nuestro programa se cierre con el boton salir
        setSize(800, 600); // Tamaño de la ventana
        setResizable(false); // El tamaño de la ventana no podrá Cambiar
        
        setLayout(new BorderLayout());
        
        panelCard();
        panelStatus(textUsuario); // Este panel siempre será visible
        
        add(pnCard, BorderLayout.CENTER);
        add(pnStatus, BorderLayout.SOUTH);
        
        setVisible(true); // Si es visible
    }
    
    /**
     * Panel de estilo cardLayout donde crearemos y insertaremos todos los demas paneles
     */
    public void panelCard()
    {
        panelUsuarios();
        panelTienda();
        panelReserva();
        panelPrincipal();
        
        pnCard= new JPanel();
        pnCard.setBorder(BorderFactory.createTitledBorder("Centro de Padel"));// Borde del cardLayout
        
        cardLayout = new CardLayout();
        pnCard.setLayout(cardLayout);
        
        pnCard.add(pnPrincipal, "Menu");
        pnCard.add(pnReserva, "Reservas");
        pnCard.add(pnTienda, "Tienda");
        pnCard.add(pnUsuario, "Usuario");
    }
    
    /**
     * Panel fijo que nos mostrara en todo momento el nombre del trabajador y la fecha 
     * @param textUsuario, Este parametro nos servira para que sepamos en todo momento el nombre del trabajador que este utilizando el programa 
     */
    public void panelStatus(String textUsuario)
    {
        JPanel statusEmp = new JPanel(new FlowLayout());
        JPanel statusFec = new JPanel(new FlowLayout());
        
        nombreEmp = new JLabel("Nombre");
        fecha = new JLabel("Fecha");
        
        statusEmp.add(nombreEmp);
        statusFec.add(fecha);

        nombreEmp.setText(textUsuario);
        fecha.setText(CentroPadelMetodos.obtenerFecha());
    
        pnStatus = new JPanel(new BorderLayout(5, 5));
        
        pnStatus.add(statusEmp, BorderLayout.WEST);
        pnStatus.add(statusFec, BorderLayout.CENTER);
    }
    
    /**
     * Panel de menú
     */
    public void panelPrincipal()
    {
        // Creamos un nuevo panel que irá dentro del cardLayout
        pnPrincipal = new JPanel();
               
        pnPrincipal.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints(); // Constructor de la tabla
        
        btnReserva = new JButton("Reserva");
        btnReserva.setPreferredSize(new Dimension(150,75));
        btnTienda = new JButton("Tienda");
        btnTienda.setPreferredSize(new Dimension(150,75));
        btnUsuario = new JButton("Usuario");
        btnUsuario.setPreferredSize(new Dimension(150,75));
        btnSalir = new JButton("Salir");
        btnSalir.setPreferredSize(new Dimension(150,75));
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        pnPrincipal.add(btnReserva, gbc);
        gbc.gridx = 0;
        gbc.gridy = 1;
        pnPrincipal.add(btnTienda, gbc);
        gbc.gridx = 0;
        gbc.gridy = 2;
        pnPrincipal.add(btnUsuario, gbc);
        gbc.gridx = 0;
        gbc.gridy = 3;
        pnPrincipal.add(btnSalir, gbc);
        
        btnReserva.addActionListener(new Oyente());
        btnSalir.addActionListener(new Oyente());
        btnTienda.addActionListener(new Oyente());
        btnUsuario.addActionListener(new Oyente());
    }
    
    /**
     * Panel de Reservas
     */
    public void panelReserva()
    {
        // Creamos una base de datos, ya que la utilizaremos en este panel
        cpbd = new CentroPadelBaseDDatos();
        
        // Creacion de componentes que vamos a necesitar
        // Componentes del panel elegir
        usuariosElegir = new JComboBox();
        tiempoElegir = new JComboBox();
        // Componentes del panel Pista
        pista1 = new JToggleButton("Pista 1");
        pista2 = new JToggleButton("Pista 2");
        pista3 = new JToggleButton("Pista 3");
        pista4 = new JToggleButton("Pista 4");
        pista5 = new JToggleButton("Pista 5");
        pista6 = new JToggleButton("Pista 6");
        pista7e = new JToggleButton("Pista 7");
        pista8e = new JToggleButton("Pista 8");
     
        cAtras1 = new JLabel("00:00");
        cAtras2 = new JLabel("00:00");
        cAtras3 = new JLabel("00:00");
        cAtras4 = new JLabel("00:00");
        cAtras5 = new JLabel("00:00");
        cAtras6 = new JLabel("00:00");
        cAtras7e = new JLabel("00:00");
        cAtras8e = new JLabel("00:00");
        // Creacion boton atras
        atras = new JButton("Atras");
        
        // Creacion de paneles que habrá en reserva
        //Panel Elegir Reserva north
        JPanel pnElegir = new JPanel();
        pnElegir.setLayout(new FlowLayout());
        // Panel Pista Reserva Center
        JPanel pnReservaC = new JPanel();
        pnReservaC.setLayout(new GridLayout(4, 4));
        // Panel atras south
        JPanel pnAtras = new JPanel();
        pnAtras.setLayout(new FlowLayout());
        // Panel contenedor Reserva
        pnReserva = new JPanel();
        pnReserva.setLayout(new BorderLayout());
        
        //
        tiempoElegir.addItem("30 Minutos");
        tiempoElegir.addItem("60 Minutos");
        tiempoElegir.addItem("120 Minutos");
        
        // cargamos la base de datos en el JComboBox correspondiente
        ArrayList<Object[]> datos = cpbd.hacerConsultaUsuarios("SELECT * FROM usuario");
        for(int i = 0; i < datos.size(); i++)
        {
            usuariosElegir.addItem(datos.get(i)[2]);
        }
        
        // Insertamos componentes en panel Elegir
        pnElegir.add(usuariosElegir);
        pnElegir.add(tiempoElegir);
        // Insertamos componentes en el panel Pista        
        pnReservaC.add(pista1);
        pnReservaC.add(pista2);
        pnReservaC.add(pista3);
        pnReservaC.add(pista4);
        pnReservaC.add(cAtras1);
        pnReservaC.add(cAtras2);
        pnReservaC.add(cAtras3);
        pnReservaC.add(cAtras4);        
        
        pnReservaC.add(pista5);
        pnReservaC.add(pista6);
        pnReservaC.add(pista7e);
        pnReservaC.add(pista8e);
        pnReservaC.add(cAtras5);
        pnReservaC.add(cAtras6);
        pnReservaC.add(cAtras7e);
        pnReservaC.add(cAtras8e);
        //Insertamos boton atras en su panel
        pnAtras.add(atras);
        
        // Insertamos paneles en reserva Contenedor
        pnReserva.add(pnElegir, BorderLayout.NORTH);
        pnReserva.add(pnReservaC, BorderLayout.CENTER);
        pnReserva.add(pnAtras, BorderLayout.SOUTH);        
        
        // Acciones de los botones
        atras.addActionListener(new Oyente());
        pista1.addActionListener(new OyenteReserva());
        pista2.addActionListener(new OyenteReserva());
        pista3.addActionListener(new OyenteReserva());
        pista4.addActionListener(new OyenteReserva());
        pista5.addActionListener(new OyenteReserva());
        pista6.addActionListener(new OyenteReserva());
        pista7e.addActionListener(new OyenteReserva());
        pista8e.addActionListener(new OyenteReserva());
    }
    
    /**
     * Panel Tienda
     */
    public void panelTienda()
    {
        // Creamos una base de datos, ya que la utilizaremos en este panel
        cpbd = new CentroPadelBaseDDatos();
        
        // Creamos los paneles que iran dentro del panel tienda
        JPanel pnProductos = new JPanel(new BorderLayout(5, 5));
        JPanel pnProductos1 = new JPanel(new FlowLayout());
        JPanel pnProductos2 = new JPanel(new FlowLayout());
        JPanel pnProductos3 = new JPanel(new FlowLayout());
        JPanel pnBotones = new JPanel(new BorderLayout(5, 5));
        JPanel pnTotal = new JPanel();
        
        // Creamos componentes que estaran en el panel tienda
        alimentacion = new JComboBox();
        deportivos = new JComboBox();
        
        JButton aniadirA = new JButton("Añadir Alimentacion");
        JButton aniadirD = new JButton("Añadir Deportivo");
        JButton fin = new JButton("Terminar");
        limpiar = new JButton("Limpiar");
        
        // cargamos la base de datos en el JComboBox correspondiente
        ArrayList<Object[]> datosD = cpbd.hacerConsultaProductos("SELECT * FROM producto WHERE tipo = 'Deportivo'");
        for(int i = 0; i < datosD.size(); i++)
        {
            deportivos.addItem(datosD.get(i)[1]);
        }
        // cargamos la base de datos en el JComboBox correspondiente
        ArrayList<Object[]> datosA = cpbd.hacerConsultaProductos("SELECT * FROM producto WHERE tipo = 'Alimentacion'");
        for(int i = 0; i < datosA.size(); i++)
        {
            alimentacion.addItem(datosA.get(i)[1]);
        }
        
        pnProductos1.add(new JLabel("Alimentos"));
        pnProductos1.add(alimentacion);
        pnProductos1.add(new JLabel("Deportivos"));
        pnProductos1.add(deportivos);
        pnProductos2.add(aniadirA);
        pnProductos2.add(aniadirD);
        pnProductos3.add(fin);
        pnProductos3.add(limpiar);
        pnBotones.add(pnProductos2, BorderLayout.CENTER);
        pnBotones.add(pnProductos3, BorderLayout.SOUTH);
        
        pnProductos.add(pnProductos1, BorderLayout.NORTH);
        pnProductos.add(pnBotones, BorderLayout.CENTER);
    
        // Creamos el panel con la tabla
        Vector columnas = new Vector();
        
        columnas.add("Unidades");
        columnas.add("Producto");
        columnas.add("Precio");
        
        tablaProductosModelo = new DefaultTableModel(null, columnas);
        
        tablaProductos = new JTable(tablaProductosModelo);
        
        JScrollPane panelTabla =new JScrollPane(tablaProductos);
        
        // Panel Atras
        JPanel pnAtras = new JPanel(new FlowLayout());
        atras = new JButton("Atras");
        
        pnAtras.add(atras);
        
        // Metemos y ordenamos todo el panel
        pnTienda = new JPanel(new BorderLayout(5, 5));
        
        pnTienda.add(panelTabla, BorderLayout.CENTER);
        pnTienda.add(pnProductos, BorderLayout.WEST);
        pnTienda.add(pnAtras, BorderLayout.SOUTH);
        
        // Acciones Botones
        atras.addActionListener(new Oyente());
        aniadirA.addActionListener(new OyenteTiendaAli());
        aniadirD.addActionListener(new OyenteTiendaDep());
        fin.addActionListener(new Oyente());
        limpiar.addActionListener(new Oyente());
    }
    
    /**
     * Panel Usuarios
     */
    public void panelUsuarios()
    {
        JPanel pnUsuarioFormulario = new JPanel(new GridLayout(6, 2));
        JPanel pnEnviar = new JPanel();
        
        nick = new JTextField();
        password = new JPasswordField();
        nombre = new JTextField();
        telefono = new JTextField();
        dni = new JTextField();
        
        // nos va a ayudar que solo haya un JRadioButton Activado
        ButtonGroup grupo= new ButtonGroup();
        
        deportista = new JRadioButton("Deportista", true);
        trabajador = new JRadioButton("Trabajador");
        
        grupo.add(deportista);
        grupo.add(trabajador);
        
        pnUsuarioFormulario.add(new JLabel("nick"));
        pnUsuarioFormulario.add(nick);
        pnUsuarioFormulario.add(new JLabel("Password"));
        pnUsuarioFormulario.add(password);
        pnUsuarioFormulario.add(new JLabel("Nombre completo"));
        pnUsuarioFormulario.add(nombre);
        pnUsuarioFormulario.add(new JLabel("Teléfono"));
        pnUsuarioFormulario.add(telefono);
        pnUsuarioFormulario.add(new JLabel("Dni"));
        pnUsuarioFormulario.add(dni);
        pnUsuarioFormulario.add(deportista);
        pnUsuarioFormulario.add(trabajador);
        
        limpiar = new JButton("Limpiar");
        JButton enviar = new JButton("Enviar");
        pnEnviar.add(limpiar);
        pnEnviar.add(enviar);
        
        // Panel Atras
        JPanel pnAtras = new JPanel(new FlowLayout());
        atras = new JButton("Atras");
        
        pnAtras.add(atras);
        
        pnUsuario = new JPanel(new BorderLayout(5, 5));
        
        pnUsuario.add(pnUsuarioFormulario, BorderLayout.NORTH);
        pnUsuario.add(pnEnviar, BorderLayout.CENTER);
        pnUsuario.add(atras, BorderLayout.SOUTH);
        
        // Acciones Botones
        atras.addActionListener(new Oyente());
        limpiar.addActionListener(new OyenteLimpiarUsuario());
        enviar.addActionListener(new OyenteEnviarUsuario());
    }
    
    /**
     * Oyente que trabaja para la mayoria de botones.
     * Botones importantes: Salir, atras. 
     * Botones para el menu: reserva, tienda, usuario
     * Botones para la tabla: terminar y limpiar
     */
    public class Oyente implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {
            if (evento.getActionCommand().equals("Reserva"))
                cardLayout.show(pnCard, "Reservas");
            if (evento.getActionCommand().equals("Tienda"))
                cardLayout.show(pnCard, "Tienda");
            if (evento.getActionCommand().equals("Usuario"))
                cardLayout.show(pnCard, "Usuario");
            if (evento.getActionCommand().equals("Atras"))
                cardLayout.show(pnCard, "Menu");
            if (evento.getActionCommand().equals("Salir"))
                System.exit(EXIT_ON_CLOSE);
            if (evento.getActionCommand().equals("Terminar"))
            {

                tablaProductos = new JTable(tablaProductosModelo);

                double total = 0;
                for(int i = 0; i < tablaProductosModelo.getRowCount(); i++)
                {
                    total += (double)tablaProductosModelo.getValueAt(i, 2);
                }
                
                JOptionPane.showMessageDialog(null,"Total a pagar " + total + " €");
                
                // Limpiamos Las filas
                int filas = tablaProductosModelo.getRowCount();
                for (int i = 0; i < filas; i++ )
                {
                    tablaProductosModelo.removeRow(0);
                }
            }
            
            if (evento.getActionCommand().equals("Limpiar"))
            {
                int filas = tablaProductosModelo.getRowCount();
                for (int i = 0; i < filas; i++ )
                {
                    tablaProductosModelo.removeRow(0);
                }
            }
        }
    }
    
    /**
     * oyente para limpiar los cuadro de texto del menu usuario
     */
    public class OyenteLimpiarUsuario implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {   
            nick.setText("");
            password.setText("");
            nombre.setText("");
            telefono.setText("");
            dni.setText("");
            
        } 
    }
    
    /**
     * oyente para enviar usuario a la base de datos
     * Este codigo debería estar implementado en un metodo a parte
     */
    public class OyenteEnviarUsuario implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {
            // Creamos una base de datos
            cpbd = new CentroPadelBaseDDatos();
            
            // Innsertamos un Usuario en la base de datos
            String tipo; // vamos a guarda el valor de JRadioButton
            if (deportista.isSelected())
                tipo = deportista.getText();
            else
                tipo = trabajador.getText();
            
            cpbd.insertar("INSERT INTO usuario VALUES(" +
                    "'"+dni.getText()+"',"+
                    "'"+nick.getText()+"',"+
                    "'"+nombre.getText()+"',"+
                    "'"+telefono.getText()+"',"+
                    "'"+password.getText()+"',"+
                    "'"+tipo+"'"+
                    " )");
            
            // Limpiamos los campos de texto
            nick.setText("");
            password.setText("");
            nombre.setText("");
            telefono.setText("");
            dni.setText("");
            
        }
    }
    
    /**
     * Tienda alimentos, nos carga la base de datos y hace consultas. 
     */
    public class OyenteTiendaAli implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {
            // Creamos una base de datos
            cpbd = new CentroPadelBaseDDatos();
            // Metemos toda la base de datos en un arrayList
            ArrayList<Object[]> datos = cpbd.hacerConsultaProductos("SELECT * FROM producto");
            
            //
            int Cantidad = 0;
            if (alimentacion.getSelectedIndex() == 0)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Bebida Isotonica"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
            if (alimentacion.getSelectedIndex() == 1)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Coca cola"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
            if (alimentacion.getSelectedIndex() == 2)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Agua 0.5"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
            if (alimentacion.getSelectedIndex() == 3)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Agua 1"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
            if (alimentacion.getSelectedIndex() == 4)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Chocolatina"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
        }
    }
    
    /**
     * oyente deportes, nos carga base de datos y hace consultas.
     */
    public class OyenteTiendaDep implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {
            // Creamos una base de datos
            cpbd = new CentroPadelBaseDDatos();
            // Metemos toda la base de datos en un arrayList
            ArrayList<Object[]> datos = cpbd.hacerConsultaProductos("SELECT * FROM producto");
            
            if (deportivos.getSelectedIndex() == 0)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Pala de Padel"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
             
            if (deportivos.getSelectedIndex() == 1)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Zapatillas"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
               
            if (deportivos.getSelectedIndex() == 2)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Pantalon"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
                 
            if (deportivos.getSelectedIndex() == 3)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Camiseta Adidas"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
               
            if (deportivos.getSelectedIndex() == 4)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Camiseta Nike"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
               
            if (deportivos.getSelectedIndex() == 5)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("Pelotas Padel"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
                
            if (deportivos.getSelectedIndex() == 6)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("30 minutos"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
              
            if (deportivos.getSelectedIndex() == 7)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("60 minutos"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
                 
            if (deportivos.getSelectedIndex() == 8)
            {
                for(int i = 0; i < datos.size(); i++)
                {
                    if(datos.get(i)[1].equals("120 minutos"))
                        tablaProductosModelo.addRow(new Object[]{"1",datos.get(i)[1],datos.get(i)[4]});
                }
            }
            
        }
    }
    
    /**
     * oyebte para reservar pista.
     */
    public class OyenteReserva implements ActionListener
    {
        @Override
        public void actionPerformed (ActionEvent evento)
        {
            CentroPadelPista cpp = new CentroPadelPista();
            if((evento.getActionCommand().equals("Pista 1")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 1, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 2")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 2, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 3")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 3, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 4")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 4, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 5")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 5, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 6")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 6, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 7")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 7, tiempoElegir.getSelectedItem().toString());
            if((evento.getActionCommand().equals("Pista 8")))
                cpp.reservarPista(usuariosElegir.getSelectedItem().toString(), 8, tiempoElegir.getSelectedItem().toString());
        }
    }
}
