

package centropadel;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Emilio García Naranjo
 */



public class CentroPadelMetodos 
{
    
    
    public CentroPadelMetodos()
    {}
    
    /**
     * MEtodo para obtener la fecha del dia
     * @return la fecha de dia
     */
    public static String obtenerFecha()
    {
        Date fecha = new Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
        
        return formatoFecha.format(fecha);
    }
        
}
