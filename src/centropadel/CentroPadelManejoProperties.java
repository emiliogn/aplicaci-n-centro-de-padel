
package centropadel;

import java.io.IOException;
import java.util.Properties;

/**
 *
 * @author Emilio García Naranjo
 */
public class CentroPadelManejoProperties 
{
    public Properties obtenerProperties()
    {
        try
        {
            //se crea una instancia a la clase Properties
            Properties propiedades = new Properties();
            //se leen el archivo .properties
            propiedades.load( getClass().getResourceAsStream("centroPAdel.properties") );
            //si el archivo de propiedades NO esta vacio retornan las propiedes leidas
            if (!propiedades.isEmpty())
            {                
                return propiedades;
            }
                else 
                {//sino  retornara NULL
                    return null;
                }
        }
        catch (IOException ioe)
        {
            return null;
        }
    }
}
