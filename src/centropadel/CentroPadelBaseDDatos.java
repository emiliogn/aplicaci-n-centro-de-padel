
package centropadel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import javax.swing.JOptionPane;

/**
 * @author Emilio García Naranjo
 */
public class CentroPadelBaseDDatos 
{
    // Enlace a la conexión de la base de datos
    Connection conexion = null;
       
    public String url;
    public String driver;
    
    public CentroPadelBaseDDatos(){}
    
    /**
     * metodo para conectar a nuestra base de datos
     */
    public void conectar()
    {
        try
        {
            // Recojo los datos del archivo .properties
            Properties mispropiedades = new CentroPadelManejoProperties().obtenerProperties();

            //se leen las propiedades indicando el KEY (identificador) y se guardan los datos de ACCESO
            url = mispropiedades.getProperty("url");
            driver = mispropiedades.getProperty("driver");

            //cargamos el Driver SQLITE
            Class.forName(driver);

            // creamos un enlace
            conexion = DriverManager.getConnection(url);
        }
        catch(ClassNotFoundException cnfe)
        {
            JOptionPane.showMessageDialog(null, cnfe);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    
    /**
     * MEtodo para hacer una consulta en la base de datos de los usuarios
     * @param consultaSql es una consulta tipo sql tipo string
     * @return un arrayList tipo Object con la consulta 
     */
    public ArrayList<Object[]> hacerConsultaUsuarios(String consultaSql)
    {
        ArrayList<Object[]> datos = new ArrayList();
        try
        {
            // Conectamos con la base de datos
            conectar();
            
            // Realizamos una consulta
            Statement consulta = conexion.createStatement();
            ResultSet rs = consulta.executeQuery(consultaSql);
            
            // Recorremos el resultado
            while (rs.next())
            {
                Object [] fila = new Object[6];
                
                fila[0]=(rs.getString(1));
                fila[1]=(rs.getString(2));
                fila[2]=(rs.getString(3));
                fila[3]=(rs.getString(4));
                fila[4]=(rs.getString(5));
                fila[5]=(rs.getString(6));
                
                datos.add(fila);
            }
            
            rs.close();
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null, se);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
        
        return datos;
    }
    
    /**
     * MEtodo para hacer una consulta en la base de datos de los productos
     * @param consultaSql es una consulta tipo sql tipo string
     * @return un arrayList tipo Object con la consulta 
     */
    public ArrayList<Object[]> hacerConsultaProductos(String consultaSql)
    {
        ArrayList<Object[]> datos = new ArrayList();
        try
        {
            // Conectamos con la base de datos
            conectar();
            
            // Realizamos una consulta
            Statement consulta = conexion.createStatement();
            ResultSet rs = consulta.executeQuery(consultaSql);
            
            // Recorremos el resultado
            while (rs.next())
            {
                Object [] fila = new Object[5];
                
                fila[0]=(rs.getInt(1)); // idProducto
                fila[1]=(rs.getString(2)); // nombre
                fila[2]=(rs.getString(3)); // tipo
                fila[3]=(rs.getInt(4)); // stock
                fila[4]=(rs.getDouble(5)); // precio
                
                datos.add(fila);
            }
            
            rs.close();
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null, se);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
        
        return datos;
    }
    
    /**
     * Metodo para hacer una insetción en la base de datos
     * @param insertSql en una consulta dipo sql
     */
    public void insertar(String insertSql)
    {
        try
        {
            // Conectamos con la base de datos
            conectar();
            
            // Realizamos una Insercion
            Statement insercion = conexion.createStatement();
            insercion.executeUpdate(insertSql);
        }
        catch(SQLException se)
        {
            JOptionPane.showMessageDialog(null, se);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e);
        }
        
    }
}
