
package centropadel;

/**
 *
 * @author Emilio García Naranjo
 */
public class CentroPadelPista 
{
    static CentroPadelBaseDDatos cpbd;
    
    private static String tarifa;

    /**
     * @return the tarifa
     */
    public static String getTarifa() {
        return tarifa;
    }

    /**
     * @param aTarifa the tarifa to set
     */
    public static void setTarifa(String aTarifa) {
        tarifa = aTarifa;
    }
    private String tipo;
    private String luz;
    private int numero;
    private String estado;

    public CentroPadelPista() {}
    
    /**
     * Este metodo tendría que ser el de cuenta atras, pero no lo consigo hacer.
     * parametros requeridos para hacer una reserva
     * @param nombre
     * @param numero
     * @param tiempo 
     */    
    public static String reservarPista(String nombre, int numero, String tiempo)
    {   
        String existo = "false";
        cpbd = new CentroPadelBaseDDatos();
        CentroPadelMetodos cpm = new CentroPadelMetodos();
        
        String socio = nombre;
        int numeroTmp = numero;
        setTarifa(tiempo);
        
        if(socio.length() > 5 && socio.length() < 50)
            if(numero >= 1 && numero < 9)
                if(tiempo.equals("30 Minutos") || tiempo.equals("60 Minutos") || tiempo.equals("120 Minutos"))
                {
                    cpbd.insertar("INSERT INTO pistareserva VALUES(" +
                            "'"+socio+"',"+
                            "'"+getTarifa()+"',"+
                            "'"+numeroTmp+"',"+
                            "'"+cpm.obtenerFecha()+"'"+
                            " )");
                    
                    existo = "true";
                }
        
        return existo;
    }
    
    /**
     * metodo que se le pasará el numero de pista, luz y estado y nos dirá el tipo de tarifa que corresponde
     * metodo donde probaremos la caja negra y junit
     * @return el tipo de tarifa que corresponde.
     */
    public String tipoTarifa(String tipo, String luz, String estado)
    {
        this.tipo = tipo;
        this.luz = luz;
        this.estado = estado;
        
        String tipoTarifa=null;
        
        if (tipo.equals("interior") || tipo.equals("exterior") && luz.equals("si") || luz.equals("no") && estado.equals("ok") || estado.equals("averiada"))
        {
            if(estado.equals("averiada"))
                tipoTarifa = "no";

            if(estado.equals("ok") && tipo.equals("interior") && luz.equals("no"))
                tipoTarifa = "normal";

            if(estado.equals("ok") && tipo.equals("interior") && luz.equals("si"))
                tipoTarifa = "alta";

            if(estado.equals("ok") && tipo.equals("exterior") && luz.equals("no"))
                tipoTarifa = "baja";

            if(estado.equals("ok") && tipo.equals("exterior") && luz.equals("si"))
                tipoTarifa = "normal";
        }
        
        return tipoTarifa;
    }
    
    /**
     * metodo que nos mostrará una cuenta atras cada vez que una pista haya sido elegida.
     */
    public void cuentaAtras(){}
    
    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the luz
     */
    public String getLuz() {
        return luz;
    }

    /**
     * @param luz the luz to set
     */
    public void setLuz(String luz) {
        this.luz = luz;
    }

    /**
     * @return the numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
